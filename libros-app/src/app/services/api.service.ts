import { Libro } from './../models/Libros.model';
import { Injectable } from '@angular/core';
import {​​​​​ HttpClient }​​​​​ from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  Url="http://localhost:8080/api/libros";
  constructor(private http: HttpClient) {
    
  }
  consultar(llave:String){
   return this.http.get<Libro[]>(this.Url+"/libros/"+llave);
  }
  allLibros(){
    return this.http.get<Libro[]>(this.Url+"/");
  }
}

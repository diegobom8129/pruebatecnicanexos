import { AppComponent } from './app.component';
import { BodyComponent } from './components/body/body.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
   {​​​​​ path: 'Consultar-Libros', component: BodyComponent }​​​​​, 
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

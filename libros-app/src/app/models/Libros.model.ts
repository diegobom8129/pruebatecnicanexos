export interface Libro {
  id: String;
  autor: any;
  editorial: String;
  genero: String;
  numPaginas: Number;
  titulo: String;
  year: String;
}

import { Libro } from './../../models/Libros.model';
import { ApiService } from './../../services/api.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
  query:String;
  resultado:Libro[]=[];
  listaLibros:Libro[]=[];
  constructor(private fb: FormBuilder,private api:ApiService) {
    this.allLibros();
  }
  consulta = this.fb.group({
    key: ''
  });
  
  buscar():void {
    console.log('Hola Mundo!');
    this.api.consultar(this.query).subscribe((res:Libro[])=>{
      console.log(res);
      this.resultado=res;
      console.log("res",this.resultado);
    }) 
  }
  allLibros():void{
    this.api.allLibros().subscribe((res:Libro[])=>{
      console.log(res);
      this.resultado=res;
      console.log("res",this.resultado);
    }) 
  }

  ngOnInit() {
  }

}

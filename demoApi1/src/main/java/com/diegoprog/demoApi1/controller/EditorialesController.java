package com.diegoprog.demoApi1.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.diegoprog.demoApi1.models.EditorialesDTO;
import com.diegoprog.demoApi1.repositories.IEditorialDAO;

@RestController
@CrossOrigin(origins="*",methods= {RequestMethod.POST,RequestMethod.GET,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api/Editoriales")
public class EditorialesController {
	
	@Autowired
	private IEditorialDAO repository;

	@PostMapping("/editoriales")
	public EditorialesDTO create(@Validated @RequestBody EditorialesDTO e) {
		EditorialesDTO editorial= new EditorialesDTO();
		if((int)e.getMaxLibrosRegistrados() == 0) {
			editorial.setNombre(e.getNombre());
			editorial.setDireccion(e.getDireccion());
			editorial.setCorreoElectronico(e.getCorreoElectronico());
			editorial.setMaxLibrosRegistrados(-1);
			editorial.setTelefono(e.getTelefono());
		}else
		{
			editorial.setNombre(e.getNombre());
			editorial.setDireccion(e.getDireccion());
			editorial.setCorreoElectronico(e.getCorreoElectronico());
			editorial.setMaxLibrosRegistrados(e.getMaxLibrosRegistrados());
			editorial.setTelefono(e.getTelefono());
		}
		
		return repository.insert(editorial);		
	}
	@GetMapping("/")
	public List<EditorialesDTO> readAll(){
		return repository.findAll();
	}
	@PutMapping("/editoriales/{id}")
	public EditorialesDTO update(@PathVariable String id,@Validated @RequestBody EditorialesDTO e) {
		return repository.save(e);
	}
	@DeleteMapping("/editoriales/{id}")
	public void delelte(@PathVariable String id) {
		repository.deleteById(id);
	}
}

package com.diegoprog.demoApi1.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.diegoprog.demoApi1.models.AutoresDTO;
import com.diegoprog.demoApi1.models.EditorialesDTO;
import com.diegoprog.demoApi1.models.LibrosDTO;
import com.diegoprog.demoApi1.repositories.IAutoresDAO;
import com.diegoprog.demoApi1.repositories.IEditorialDAO;
import com.diegoprog.demoApi1.repositories.ILibrosDAO;

@RestController
@CrossOrigin(origins="*",methods= {RequestMethod.POST,RequestMethod.GET,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api/libros")
public class LibrosController {
	private EditorialesDTO editorial= new EditorialesDTO();
	private AutoresDTO autores= new AutoresDTO();
	private LibrosDTO libros= new LibrosDTO();
	private List<EditorialesDTO> listEditorial= new ArrayList<EditorialesDTO>();
	private List<AutoresDTO> listAutores= new ArrayList<AutoresDTO>();
	private List<LibrosDTO> listLibros= new ArrayList<LibrosDTO>();
	@Autowired
	private ILibrosDAO repositoryLibros;
	@Autowired
	private IAutoresDAO repositoryAutores;
	@Autowired
	private IEditorialDAO repositoryEditoriales;
	
	@PostMapping("/libros")
	public String create(@Validated @RequestBody LibrosDTO libs) {
		String respuesta="";
		int nuevoLibro=0;
		String titl=libs.getTitulo();
		String autor=libs.getAutor().getNombreCompleto();
		String editori=libs.getEditorial();
		
		this.listEditorial=this.repositoryEditoriales.findAll();
		this.listLibros=this.repositoryLibros.findAll();
		this.listAutores=this.repositoryAutores.findAll();
		if(repositoryLibros.findByTitulo(titl).size()!=0) {
			for(EditorialesDTO e:listEditorial) {
	             if(e.getNombre().equals(editori) ){
	            	 nuevoLibro=(int)e.getMaxLibrosRegistrados()+1;
	            	if((int)e.getMaxLibrosRegistrados()<=nuevoLibro) {
	            		respuesta="El número de existencias de esta editorial exede el permitido";
	            	}	 
	             }
			}
		}else {
			for(EditorialesDTO e:listEditorial) {
	             if(e.getNombre().equals(editori) ){
	            	 nuevoLibro=(int)e.getMaxLibrosRegistrados()+1;
	            	if((int)e.getMaxLibrosRegistrados()<=nuevoLibro) {
	            		respuesta="El número de existencias de esta editorial exede el permitido";
	            	}	 
	             }
	             if(repositoryEditoriales.findByNombre(editori).size()== 0 )
						respuesta="La editorial no está registrada";
			}
			for(AutoresDTO itemAutor:listAutores) {
				if(!itemAutor.getNombreCompleto().equals(autor))
					respuesta="El autor no está registrao";
			}
			
				
			
				
		}
		if(respuesta =="") {
			repositoryLibros.insert(libs);
			respuesta="Libro registrado con exito";
		}
		return respuesta;
	}
	@GetMapping("/")
	public List<LibrosDTO> readAll(){
		return repositoryLibros.findAll();
	}
	@PutMapping("/libros/{id}")
	public LibrosDTO update(@PathVariable String id,@Validated @RequestBody LibrosDTO l) {
		
		return repositoryLibros.save(l);
	}
	@DeleteMapping("/libros/{id}")
	public void delelte(@PathVariable String id) {
		repositoryLibros.deleteById(id);
	}
	@GetMapping("/libros/{clave}")
	public List<LibrosDTO> buscarXclave(@PathVariable(name="clave") String clave){
		List<LibrosDTO> libros= new ArrayList<>();
		libros= repositoryLibros.findByTitulo(clave);
		if(libros.size()==0) {
			libros=repositoryLibros.findByYear(clave);
			if(libros.size()==0) {	
		
			}
		}
		return libros;
	}
}

package com.diegoprog.demoApi1.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.diegoprog.demoApi1.models.AutoresDTO;
import com.diegoprog.demoApi1.repositories.IAutoresDAO;

@RestController
@CrossOrigin(origins="*",methods= {RequestMethod.POST,RequestMethod.GET,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api/Autores")
public class AutoresController {
	
	@Autowired
	private IAutoresDAO repository;
	
	@PostMapping("/autores")
	public AutoresDTO create(@Validated @RequestBody AutoresDTO a) {
		return repository.insert(a);		
	}
	@GetMapping("/")
	public List<AutoresDTO> readAll(){
		return repository.findAll();
	}
	@PutMapping("/autores/{id}")
	public AutoresDTO update(@PathVariable String id,@Validated @RequestBody AutoresDTO a) {
		return repository.save(a);
	}
	@DeleteMapping("/autores/{id}")
	public void delelte(@PathVariable String id) {
		repository.deleteById(id);
	}

}

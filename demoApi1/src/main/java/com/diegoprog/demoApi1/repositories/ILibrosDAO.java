package com.diegoprog.demoApi1.repositories;


import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


import com.diegoprog.demoApi1.models.LibrosDTO;

@Repository
public interface ILibrosDAO extends MongoRepository<LibrosDTO, String>{

	  public List<LibrosDTO> findByTitulo(String titulo);
	  public List<LibrosDTO> findByYear(String clave);
	
}

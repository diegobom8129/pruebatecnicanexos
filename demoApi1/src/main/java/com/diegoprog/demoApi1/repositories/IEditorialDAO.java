package com.diegoprog.demoApi1.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.diegoprog.demoApi1.models.*;
@Repository
public interface IEditorialDAO extends MongoRepository<EditorialesDTO, String>  {
	public List<IEditorialDAO> findByNombre(String titulo);
}

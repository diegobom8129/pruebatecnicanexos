package com.diegoprog.demoApi1.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.diegoprog.demoApi1.models.AutoresDTO;

@Repository
public interface IAutoresDAO extends MongoRepository<AutoresDTO, String> {
  
    
}

package com.diegoprog.demoApi1.models;



import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.lang.NonNull;



@Document(collection="Libros")
public class LibrosDTO {
	@org.springframework.data.annotation.Id
	private String Id;
	@NotNull
	private String titulo;
	private String year;
	@NotNull
	private String genero;
	@NotNull
	private  Double numPaginas;
	private String editorial;
	@NotNull
	private AutoresDTO autor;
	
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public Double getNumPaginas() {
		return numPaginas;
	}
	public void setNumPaginas(Double numPaginas) {
		this.numPaginas = numPaginas;
	}
	public String getEditorial() {
		return editorial;
	}
	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}
	public void setAutor(AutoresDTO autor) {
		this.autor = autor;
	}
	public AutoresDTO getAutor() {
		return autor;
	}
	


	
	
	
	
	
}

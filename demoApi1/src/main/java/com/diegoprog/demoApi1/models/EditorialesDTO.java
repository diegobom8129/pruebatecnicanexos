package com.diegoprog.demoApi1.models;

import java.time.LocalDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.validation.constraints.NotNull;



@Document(collection="Editoriales")
public class EditorialesDTO {

	private String id;
	@NotNull
	private String nombre;
	@NotNull
	private String direccion;
	@NotNull
	private int telefono;
	private String correoElectronico;
	@NotNull
	private double maxLibrosRegistrados;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public int getTelefono() {
		return telefono;
	}
	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	public double getMaxLibrosRegistrados() {
		return maxLibrosRegistrados;
	}
	public void setMaxLibrosRegistrados(double maxLibrosRegistrados) {
		this.maxLibrosRegistrados = maxLibrosRegistrados;
	}
}

package com.diegoprog.demoApi1.models;
import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.lang.NonNull;



@Document(collection="Autores")
public class AutoresDTO {

	@Id
	private String id;
	@NotNull
	private String nombreCompleto;
	private LocalDate fechaNacimiento;
	private String ciudadProcedencia;
	private String correoElectorinico;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getCiudadProcedencia() {
		return ciudadProcedencia;
	}
	public void setCiudadProcedencia(String ciudadProcedencia) {
		this.ciudadProcedencia = ciudadProcedencia;
	}
	public String getCorreoElectorinico() {
		return correoElectorinico;
	}
	public void setCorreoElectorinico(String correoElectorinico) {
		this.correoElectorinico = correoElectorinico;
	}
	
}
